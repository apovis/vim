call plug#begin()
" The default plugin directory will be as follows:
"   - Vim (Linux/macOS): '~/.vim/plugged'
"   - Vim (Windows): '~/vimfiles/plugged'
"   - Neovim (Linux/macOS/Windows): stdpath('data') . '/plugged'
" You can specify a custom plugin directory by passing it as the argument
"   - e.g. `call plug#begin('~/.vim/plugged')`
"   - Avoid using standard Vim directory names like 'plugin'

" Make sure you use single quotes

" Shorthand notation; fetches https://github.com/junegunn/vim-easy-align
"Plug 'junegunn/vim-easy-align'

" Any valid git URL is allowed
"Plug 'https://github.com/junegunn/vim-github-dashboard.git'

" Multiple Plug commands can be written in a single line using | separators
"Plug 'SirVer/ultisnips' | Plug 'honza/vim-snippets'

" nerdtree - ide like setting
Plug 'https://github.com/scrooloose/nerdtree.git'

" more git reletated things
Plug 'https://github.com/airblade/vim-gitgutter.git'

" comments for vim
Plug 'https://github.com/tpope/vim-commentary.git'

" nice git bar at the bottom
Plug 'https://github.com/bling/vim-airline.git'

" git commands for vim
Plug 'https://github.com/tpope/vim-fugitive.git'

" syntax checking and semantic errors
Plug 'https://github.com/w0rp/ale.git'

" all about surroundings
Plug 'https://github.com/tpope/vim-surround.git'

" use <Tab> for all your insert completion needs
Plug 'https://github.com/ervandew/supertab.git'

" snippets for various languages
Plug 'https://github.com/honza/vim-snippets.git'

" syntax checker
Plug 'https://github.com/scrooloose/syntastic.git'

" solarized color schema
"Plug 'https://github.com/altercation/vim-colors-solarized.git'

" If you are using Vim-Plug
Plug 'https://github.com/shaunsingh/solarized.nvim'
"Plug 'shaunsingh/solarized.nvim'

" ansible for vim
Plug 'https://github.com/pearofducks/ansible-vim.git'

call plug#end()

syntax enable
"set background=dark
set background=light
colorscheme solarized

set nocompatible                " Use Vim defaults instead of 100% vi compatibility
set backspace=indent,eol,start  " more powerful backspacing
set history=150                 " keep 50 lines of command line history
set ruler                       " show the cursor position all the time
set cursorcolumn                " set vertical ruler
set nu                          " set line numbers
set visualbell                  " Blink cursor on error instead of beeping
set ttyfast                     " rendering

" no history and keep it in ~/.cache/nvim
let g:netrw_dirhistmax = 0
let g:netrw_home=$XDG_CACHE_HOME.'/nvim'

" delay before writing to swap file
set updatetime=1000

" set parentheses recognition
set sm
set smd

" better copy/paste behavior
set paste

" Better command-line completion
set wildmenu
set wildmode=longest:full,full

" set default airline theme
let g:airline_solarized_bg='light'


let mapleader = ","
map <leader>h :noh<CR>
map <leader>li :set list!<CR>    " Toggle tabs and EOL
map <leader>bg :let &background = ( &background == "dark"? "light" : "dark" )<CR>
"map <leader>gb :let &airline_solarized_bg = ( &airline_solarized_bg == "dark"? "light" : "dark" )<CR>
"map <leader>v :vsplit<CR>
"map <leader>h :ssplit<CR>
"map <leader>se :Sex<CR> "split (v) and open explorer
"map <leader>rw :%s/\s\+$//e<CR> "remove trailing whitespace

"set mouse=nvi

"set listchars=tab:▸\ ,eol:¬,trail:•,extends:#,nbsp:.
set listchars=eol:¶,space:˽,tab:֍\ ,extends:›,precedes:‹,nbsp:·,trail:·

" recognize files
filetype on
filetype indent on
filetype plugin on

" Ignore case when searching
set ignorecase
set smartcase

" Highlight search results
set incsearch           " search as characters are entered
set hlsearch            " highlight matches

" No annoying sound on errors
set noerrorbells
"set novisualbell
set t_vb=
set tm=500

" Set utf8 as standard encoding and en_US as the standard language
set encoding=utf8

" spelling
"set spell
"set spelllang=de_de

" Use Unix as the standard file type
set ffs=unix,dos,mac

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Text, tab and indent related
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Be smart when using tabs ;)
set smarttab

" 1 tab == 4 spaces
set shiftwidth=2
set tabstop=2			" number of visual spaces per TAB
set softtabstop=2		" number of spaces in tab when editing
set expandtab			" tabs are spaces

set cursorline          " highlight current line
set wildmenu            " visual autocomplete for command menu
set lazyredraw          " redraw only when we need to.
set showmatch           " highlight matching [{()}]


" Linebreak on 500 characters
set lbr
set tw=500

set ai "Auto indent
set si "Smart indent
set wrap "Wrap lines

" folding related stuff
"set foldlevelstart=1
set nofoldenable
setlocal foldmethod=indent

" powerline icons for airline
let g:airline_powerline_fonts=1

" list all open tabs or buffers
let g:airline#extensions#tabline#enabled=1

""""""""""""""""""""""""""""""
" => Visual mode related
""""""""""""""""""""""""""""""
" Visual mode pressing * or # searches for the current selection
" Super useful! From an idea by Michael Naumann
vnoremap <silent> * :call VisualSelection('f')<CR>
vnoremap <silent> # :call VisualSelection('b')<CR>
""""""""""""""""""""""""""""""

" Bubble single lines
nmap <C-Up> [e
nmap <C-Down> ]e
" Bubble multiple lines
vmap <C-Up> [egv
vmap <C-Down> ]egv

" encryption
"set cm=blowfish2

" highlight last inserted text
nnoremap gV `[v`]

" split vertical, horizontal
nnoremap ,v <C-w>v
nnoremap ,h <C-w>s
nnoremap ,l <C-w>l
nnoremap ,h <C-w>h
nnoremap ,j <C-w>j
nnoremap ,k <C-w>k

nnoremap th  :tabfirst<CR>
nnoremap tk  :tabnext<CR>
nnoremap <C-Tab>  :tabnext<CR>
nnoremap tj  :tabprev<CR>
nnoremap tl  :tablast<CR>
nnoremap te  :tabedit<Space>
nnoremap tn  :tabnew<Space>
nnoremap <C-t> :tabnew<CR>
nnoremap tm  :tabm<Space>
nnoremap td  :tabclose<CR>
nnoremap to  :tabonly<CR>


" remove trailing whitespace
nnoremap <silent> <F5> :let _s=@/ <Bar> :%s/\s\+$//e <Bar> :let @/=_s <Bar> :nohl <Bar> :unlet _s <CR>
"autocmd BufWritePre * :%s/\s\+$//e
